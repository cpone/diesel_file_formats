debug_prints_enabled = True
def debug_print(*args):
	if debug_prints_enabled:
		print(*args)

import zlib
import struct

from xml.etree.ElementTree import Element, SubElement, Comment, tostring

class DieselScriptDataEnums():
	ITEM_NULL = 0
	ITEM_FALSE = 1
	ITEM_TRUE = 2
	ITEM_NUMBER = 3
	ITEM_STRING = 4
	ITEM_VECTOR = 5
	ITEM_QUATERNION = 6
	ITEM_IDSTRING = 7
	ITEM_TABLE = 8

	generic_types = [
		"null",
		"boolean",
		"boolean",
		"number",
		"string",
		"vector3",
		"quaternion",
		"idstring",
		"table"
	]

class DieselScriptDataStructs():
	endian = "<"

	# Header Structs
	header_32bit = struct.Struct(endian + "IIIiIIIiIIIiIIIiIIIiIIIixxxxI")
	header_64bit = struct.Struct(endian + "IQQqIQQqIQQqIQQqIQQqIQQqxxxxI")

	# Item Structs
	number = struct.Struct(endian + "f")

	string_offset_32bit = struct.Struct(endian + "xxxxi")
	string_offset_64bit = struct.Struct(endian + "xxxxxxxxq")

	vector = struct.Struct(endian + "fff")

	quaternion = struct.Struct(endian + "ffff")

	idstring = struct.Struct(endian + "Q")

	table_info_32bit = struct.Struct(endian + "iIIixxxx")
	table_info_64bit = struct.Struct(endian + "qQQqxxxxxxxx")

	item_info = struct.Struct(endian + "II")

	weird_file_ending = struct.Struct(endian + "I")

class DieselScriptData():
	def __init__(self):
		self.root_type = 0
		self.root_index = 0

		self.numbers = []
		self.strings = []
		self.vectors = []
		self.quaternions = []
		self.idstrings = []
		self.tables = []

	def __read_header(self, scriptdata_data, is_64bit):
		header_unpacker = DieselScriptDataStructs.header_32bit
		if is_64bit:
			header_unpacker = DieselScriptDataStructs.header_64bit

		header_data = header_unpacker.unpack(scriptdata_data[0:header_unpacker.size])

		return header_data

	def __read_data(self, header_data_tuple, scriptdata_data, is_64bit):
		##############################
		# 32 Bit or 64 Bit Unpackers #
		##############################

		number_unpacker = DieselScriptDataStructs.number
		string_offset_unpacker = DieselScriptDataStructs.string_offset_32bit
		vector_unpacker = DieselScriptDataStructs.vector
		quaternion_unpacker = DieselScriptDataStructs.quaternion
		idstring_unpacker = DieselScriptDataStructs.idstring
		table_info_unpacker = DieselScriptDataStructs.table_info_32bit
		item_info_unpacker = DieselScriptDataStructs.item_info
		weird_file_ending_unpacker = DieselScriptDataStructs.weird_file_ending

		if is_64bit:
			number_unpacker = DieselScriptDataStructs.number
			string_offset_unpacker = DieselScriptDataStructs.string_offset_64bit
			vector_unpacker = DieselScriptDataStructs.vector
			quaternion_unpacker = DieselScriptDataStructs.quaternion
			idstring_unpacker = DieselScriptDataStructs.idstring
			table_info_unpacker = DieselScriptDataStructs.table_info_64bit
			item_info_unpacker = DieselScriptDataStructs.item_info
			weird_file_ending_unpacker = DieselScriptDataStructs.weird_file_ending

		##########
		# HEADER #
		##########

		def _verify_count(name, count, count_alt):
			if count != count_alt:
				print("Header Read Failed! {} counts don't match. ({},{})".format(name, count, count_alt))
				return False
			return True

		debug_print("\nHeader Data:")

		number_tag = header_data_tuple[0]
		number_count = header_data_tuple[1]
		number_count_alt = header_data_tuple[2]
		number_offset = header_data_tuple[3]
		debug_print("\tNumbers:")
		debug_print("\t\tTag: {}".format(number_tag))
		debug_print("\t\tCount: {}".format(number_count))
		debug_print("\t\tCount Alt: {}".format(number_count_alt))
		debug_print("\t\tOffset: {}".format(number_offset))
		if not _verify_count("Number", number_count, number_count_alt):
			return

		string_tag = header_data_tuple[4]
		string_count = header_data_tuple[5]
		string_count_alt = header_data_tuple[6]
		string_offset = header_data_tuple[7]
		debug_print("\tStrings:")
		debug_print("\t\tTag: {}".format(string_tag))
		debug_print("\t\tCount: {}".format(string_count))
		debug_print("\t\tCount Alt: {}".format(string_count_alt))
		debug_print("\t\tOffset: {}".format(string_offset))
		if not _verify_count("String", string_count, string_count_alt):
			return

		vector_tag = header_data_tuple[8]
		vector_count = header_data_tuple[9]
		vector_count_alt = header_data_tuple[10]
		vector_offset = header_data_tuple[11]
		debug_print("\tVectors:")
		debug_print("\t\tTag: {}".format(vector_tag))
		debug_print("\t\tCount: {}".format(vector_count))
		debug_print("\t\tCount Alt: {}".format(vector_count_alt))
		debug_print("\t\tOffset: {}".format(vector_offset))
		if not _verify_count("Vector", vector_count, vector_count_alt):
			return

		quaternion_tag = header_data_tuple[12]
		quaternion_count = header_data_tuple[13]
		quaternion_count_alt = header_data_tuple[14]
		quaternion_offset = header_data_tuple[15]
		debug_print("\tQuaternions:")
		debug_print("\t\tTag: {}".format(quaternion_tag))
		debug_print("\t\tCount: {}".format(quaternion_count))
		debug_print("\t\tCount Alt: {}".format(quaternion_count_alt))
		debug_print("\t\tOffset: {}".format(quaternion_offset))
		if not _verify_count("Quaternion", quaternion_count, quaternion_count_alt):
			return

		idstring_tag = header_data_tuple[16]
		idstring_count = header_data_tuple[17]
		idstring_count_alt = header_data_tuple[18]
		idstring_offset = header_data_tuple[19]
		debug_print("\tIDStrings:")
		debug_print("\t\tTag: {}".format(idstring_tag))
		debug_print("\t\tCount: {}".format(idstring_count))
		debug_print("\t\tCount Alt: {}".format(idstring_count_alt))
		debug_print("\t\tOffset: {}".format(idstring_offset))
		if not _verify_count("IDString", idstring_count, idstring_count_alt):
			return

		table_tag = header_data_tuple[20]
		table_count = header_data_tuple[21]
		table_count_alt = header_data_tuple[22]
		table_offset = header_data_tuple[23]
		debug_print("\tTables:")
		debug_print("\t\tTag: {}".format(table_tag))
		debug_print("\t\tCount: {}".format(table_count))
		debug_print("\t\tCount Alt: {}".format(table_count_alt))
		debug_print("\t\tOffset: {}".format(table_offset))
		if not _verify_count("Table", table_count, table_count_alt):
			return

		self.root_type = (header_data_tuple[24] >> 24) & 0xFFFFFF
		self.root_index = header_data_tuple[24] & 0xFFFFFF

		debug_print("\tRoot Item:")
		debug_print("\t\tType: {}".format(self.root_type))
		debug_print("\t\tIndex: {}".format(self.root_index))

		# Generic data reader.
		def _read_data(count, unpacker, offset):
			output = []

			size = count * unpacker.size
			data_iterator = unpacker.iter_unpack(scriptdata_data[offset:offset+size])

			for data in data_iterator:
				if len(data) == 1:
					output.append(data[0])
				else:
					output.append(tuple(data))

			return output

		# Null terminated string reader.
		def _read_string(offset):
			temp_string = ""
			for byte in scriptdata_data[offset:]:
				if byte == 0:
					return temp_string

				temp_string += chr(byte)

		debug_print("\nData:")

		self.numbers = _read_data(number_count, number_unpacker, number_offset)

		debug_print("\tNumbers:")
		debug_print("\t\t{}".format(self.numbers))

		self.strings = []

		string_offsets_size = string_count * string_offset_unpacker.size
		string_offsets_data_iterator = string_offset_unpacker.iter_unpack(scriptdata_data[string_offset:string_offset+string_offsets_size])

		for string_offset_data in string_offsets_data_iterator:
			string_offset = string_offset_data[0]
			string = _read_string(string_offset)

			self.strings.append(string)

		debug_print("\tStrings:")
		debug_print("\t\t{}".format(self.strings))

		self.vectors = _read_data(vector_count, vector_unpacker, vector_offset)

		debug_print("\tVectors:")
		debug_print("\t\t{}".format(self.vectors))

		self.quaternions = _read_data(quaternion_count, quaternion_unpacker, quaternion_offset)

		debug_print("\tQuaternions:")
		debug_print("\t\t{}".format(self.quaternions))

		self.idstrings = _read_data(idstring_count, idstring_unpacker, idstring_offset)

		debug_print("\tIDStrings:")
		debug_print("\t\t{}".format(self.idstrings))


		def _get_string_index(target_string):
			for index, string in enumerate(self.strings):
				if target_string == string:
					return index

		self.tables = []

		tabel_size = 0

		table_infos_size = table_count * table_info_unpacker.size
		table_infos_data_iterator = table_info_unpacker.iter_unpack(scriptdata_data[table_offset:table_offset+table_infos_size])

		tabel_size += table_infos_size

		for table_info_data in table_infos_data_iterator:
			metatable_offset = table_info_data[0]
			count = table_info_data[1]
			count_alt = table_info_data[2]
			items_offset = table_info_data[3]

			meta = None
			if metatable_offset > -1:
				meta = _get_string_index(_read_string(metatable_offset))

			if not _verify_count("Table Items", count, count_alt):
				return

			table = (meta, [])

			item_infos_size = count * item_info_unpacker.size
			item_infos_data_iterator = item_info_unpacker.iter_unpack(scriptdata_data[items_offset:items_offset+item_infos_size])

			tabel_size += item_infos_size

			for item_info_data in item_infos_data_iterator:
				key_index = item_info_data[0] & 0xFFFFFF;
				key_type = (item_info_data[0] >> 24) & 0xFF;
				item_index = item_info_data[1] & 0xFFFFFF;
				item_type = (item_info_data[1] >> 24) & 0xFF;

				table[1].append((key_type, key_index, item_type, item_index))

			self.tables.append(table)

		debug_print("\tTables:")
		debug_print("\t\t{}".format(self.tables))

		debug_print("\nWeird File Ending: {}".format(weird_file_ending_unpacker.unpack(scriptdata_data[-4:])[0]))

	def read(self, filepath):
		debug_print("\nReading Diesel ScriptData: {}".format(filepath))

		scriptdata_file = open(filepath, 'rb')
		scriptdata_data = scriptdata_file.read()
		scriptdata_file.close()

		##################
		# Read File Data #
		##################
		is_64bit = False

		if len(scriptdata_data) < 104:
			debug_print("\nCompletely Invalid ScriptData File! Less than 104 bytes!")
			return

		header_data_tuple = self.__read_header(scriptdata_data, False)

		# Check if header is valid as a 64 bit check.
		header_tag = header_data_tuple[0]

		# If they don't match read 64 bit header and check if that is valid.
		if header_tag == 568494624:
			debug_print("\n32 Bit Failure")

			if len(scriptdata_data) < 156:
				debug_print("\nCompletely Invalid ScriptData File! Less than 52 bytes!")
				return

			header_data_tuple = self.__read_header(scriptdata_data, True)

			is_64bit = True

		# Now read the data.
		self.__read_data(header_data_tuple, scriptdata_data, is_64bit)

	def write(self, filepath, is_64bit=False):
		debug_print("\nWriting Diesel ScriptData: {}".format(filepath))

		##############################
		# 32 Bit or 64 Bit Packers #
		##############################

		header_packer = DieselScriptDataStructs.header_32bit

		number_packer = DieselScriptDataStructs.number
		string_offset_packer = DieselScriptDataStructs.string_offset_32bit
		vector_packer = DieselScriptDataStructs.vector
		quaternion_packer = DieselScriptDataStructs.quaternion
		idstring_packer = DieselScriptDataStructs.idstring
		table_info_packer = DieselScriptDataStructs.table_info_32bit
		item_info_packer = DieselScriptDataStructs.item_info
		weird_file_ending_packer = DieselScriptDataStructs.weird_file_ending

		if is_64bit:
			header_packer = DieselScriptDataStructs.header_64bit

			number_packer = DieselScriptDataStructs.number
			string_offset_packer = DieselScriptDataStructs.string_offset_64bit
			vector_packer = DieselScriptDataStructs.vector
			quaternion_packer = DieselScriptDataStructs.quaternion
			idstring_packer = DieselScriptDataStructs.idstring
			table_info_packer = DieselScriptDataStructs.table_info_64bit
			item_info_packer = DieselScriptDataStructs.item_info
			weird_file_ending_packer = DieselScriptDataStructs.weird_file_ending

		#################
		# Section Sizes #
		#################
		debug_print("\nSection Sizes:")

		header_size = header_packer.size

		numbers_count = len(self.numbers)
		numbers_size = numbers_count * number_packer.size

		strings_count = len(self.strings)
		strings_size = strings_count * string_offset_packer.size
		for string in self.strings:
			strings_size += len(string) + 1

		vectors_count = len(self.vectors)
		vectors_size = vectors_count * vector_packer.size

		quaternions_count = len(self.quaternions)
		quaternions_size = quaternions_count * quaternion_packer.size

		idstrings_count = len(self.idstrings)
		idstrings_size = idstrings_count * idstring_packer.size

		tables_count = len(self.tables)
		tables_size = tables_count * table_info_packer.size
		for table in self.tables:
			tables_size += len(table[1]) * item_info_packer.size

		weird_file_ending_size = weird_file_ending_packer.size

		total_size = header_size + numbers_size + strings_size + vectors_size + quaternions_size + idstrings_size + tables_size + weird_file_ending_size

		debug_print("\tHeader Size: {}".format(header_size))
		debug_print("\tNumbers Size: {}".format(numbers_size))
		debug_print("\tStrings Size: {}".format(strings_size))
		debug_print("\tVectors Size: {}".format(vectors_size))
		debug_print("\tQuaternions Size: {}".format(quaternions_size))
		debug_print("\tIDStrings Size: {}".format(idstrings_size))
		debug_print("\tTables Size: {}".format(tables_size))
		debug_print("\tWeird File Ending Size: {}".format(weird_file_ending_size))

		debug_print("\n\tTotal Size: {}".format(total_size))

		###################
		# Section Offsets #
		###################
		debug_print("\nSection Offsets:")

		numbers_offset = header_size
		strings_offset = numbers_offset + numbers_size
		vectors_offset = strings_offset + strings_size
		quaternions_offset = vectors_offset + vectors_size
		idstrings_offset = quaternions_offset + quaternions_size
		tables_offset = idstrings_offset + idstrings_size

		debug_print("\tNumbers Offset: {}".format(numbers_offset))
		debug_print("\tStrings Offset: {}".format(strings_offset))
		debug_print("\tVectors Offset: {}".format(vectors_offset))
		debug_print("\tQuaternions Offset: {}".format(quaternions_offset))
		debug_print("\tIDStrings Offset: {}".format(idstrings_offset))
		debug_print("\tTables Offset: {}".format(tables_offset))

		scriptdata_data = bytearray(total_size)

		####################
		# Pack Header Data #
		####################
		tag = 0
		if is_64bit:
			tag = 568494624

		packed_root = ((self.root_type & 0xFF) << 24) + (self.root_index & 0xFFFFFF)

		header_packer.pack_into(scriptdata_data, 0,
			tag,
			numbers_count,
			numbers_count,
			numbers_offset,
			tag,
			strings_count,
			strings_count,
			strings_offset,
			tag,
			vectors_count,
			vectors_count,
			vectors_offset,
			tag,
			quaternions_count,
			quaternions_count,
			quaternions_offset,
			tag,
			idstrings_count,
			idstrings_count,
			idstrings_offset,
			tag,
			tables_count,
			tables_count,
			tables_offset,
			packed_root
		)

		###################
		# Section Writing #
		###################

		# Generic data writer.
		def _write_data(data, packer, offset):
			output = []

			for index, data_piece in enumerate(data):
				if type(data_piece) == tuple:
					packer.pack_into(scriptdata_data, offset + (index * packer.size), *data_piece)
				else:
					packer.pack_into(scriptdata_data, offset + (index * packer.size), data_piece)

		# Null terminated string writer.
		def _write_strings(offset, strings):
			current_index = offset
			offsets = {}

			for string in strings:
				offsets[string] = current_index

				for character in string:
					scriptdata_data[current_index] = ord(character)
					current_index += 1
				scriptdata_data[current_index] = 0
				current_index += 1

			return offsets	

		################
		# Pack Numbers #
		################

		_write_data(self.numbers, number_packer, numbers_offset)

		################
		# Pack Strings #
		################

		string_offsets_end = strings_offset + (strings_count * string_offset_packer.size)
		current_string_offset = string_offsets_end

		for index, string in enumerate(self.strings):
			string_offset_packer.pack_into(scriptdata_data, strings_offset + (index * string_offset_packer.size), current_string_offset)
			current_string_offset += len(string) + 1

		packed_string_offsets = _write_strings(string_offsets_end, self.strings)

		################
		# Pack Vectors #
		################

		_write_data(self.vectors, vector_packer, vectors_offset)

		####################
		# Pack Quaternions #
		####################

		_write_data(self.quaternions, quaternion_packer, quaternions_offset)

		####################
		# Pack IDStrings #
		####################

		_write_data(self.idstrings, idstring_packer, idstrings_offset)

		###############
		# Pack Tables #
		###############

		table_infos_end = tables_offset + (tables_count * table_info_packer.size)
		current_table_items_offset = table_infos_end

		for index, table in enumerate(self.tables):
			meta = table[0]
			items = table[1]

			metatable_offset = -1
			if meta != None:
				metatable_offset = packed_string_offsets[self.strings[meta]]
			items_count = len(items)

			table_info_packer.pack_into(scriptdata_data, tables_offset + (index * table_info_packer.size),
				metatable_offset,
				items_count,
				items_count,
				current_table_items_offset
			)

			for index, item in enumerate(items):
				packed_key = ((item[0] & 0xFF) << 24) + (item[1] & 0xFFFFFF)
				packed_item = ((item[2] & 0xFF) << 24) + (item[3] & 0xFFFFFF)

				item_info_packer.pack_into(scriptdata_data, current_table_items_offset + (index * item_info_packer.size),
					packed_key,
					packed_item
				)

			current_table_items_offset += items_count * item_info_packer.size

		###########################
		# Add Weird Ending Number #
		###########################

		weird_file_ending_packer.pack_into(scriptdata_data, total_size-4, 1836436848)

		scriptdata_file = open(filepath, 'wb')
		scriptdata_file.write(scriptdata_data)
		scriptdata_file.close()

	@property
	def generic_xml(self):
		def _get_data(data_type, data_index):
			if data_type == DieselScriptDataEnums.ITEM_NULL:
				return "null"
			elif data_type == DieselScriptDataEnums.ITEM_FALSE:
				return False
			elif data_type == DieselScriptDataEnums.ITEM_TRUE:
				return True
			elif data_type == DieselScriptDataEnums.ITEM_NUMBER:
				return self.numbers[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_STRING:
				return self.strings[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_VECTOR:
				return self.vectors[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_QUATERNION:
				return self.quaternions[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_IDSTRING:
				return self.idstrings[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_TABLE:
				return self.tables[data_index]

		def _generate_tree(key_type, key_index, data_type, data_index, parent):
			if parent != None:
				key = _get_data(key_type, key_index)
				key_key = "key"

				if key_type == DieselScriptDataEnums.ITEM_NUMBER:
					key = int(key)
					key_key = "index"
				key = str(key)

			element = None
			if parent != None:
				element = SubElement(parent, "entry")
				element.set(key_key, key)
			else:
				element = Element("generic_scriptdata")

			element.set("type", DieselScriptDataEnums.generic_types[data_type])

			data = _get_data(data_type, data_index)

			if data_type != DieselScriptDataEnums.ITEM_TABLE:
				if type(data) is tuple:
					data = map(str, data)
					data = " ".join(data)
				elif type(data) is bool:
					data = str(data).lower()
				elif type(data) is float:
					if data.is_integer():
						data = int(data)
					data = str(data)
				else:
					data = str(data)

				element.set("value", data)
			else:
				if data[0] != -1:
					element.set("_meta", data[0])

				for item_info in data[1]:
					_generate_tree(item_info[0], item_info[1], item_info[2], item_info[3], element)

			return element

		data = _get_data(self.root_type, self.root_index)

		root_element = _generate_tree(None, None, self.root_type, self.root_index, None)

		return tostring(root_element).decode("utf-8")

	@generic_xml.setter
	def generic_xml(self, value):
		None

	@property
	def custom_xml(self):
		def _get_data(data_type, data_index):
			if data_type == DieselScriptDataEnums.ITEM_NULL:
				return "null"
			elif data_type == DieselScriptDataEnums.ITEM_FALSE:
				return False
			elif data_type == DieselScriptDataEnums.ITEM_TRUE:
				return True
			elif data_type == DieselScriptDataEnums.ITEM_NUMBER:
				return self.numbers[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_STRING:
				return self.strings[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_VECTOR:
				return self.vectors[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_QUATERNION:
				return self.quaternions[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_IDSTRING:
				return self.idstrings[data_index]
			elif data_type == DieselScriptDataEnums.ITEM_TABLE:
				return self.tables[data_index]

		def _generate_tree(key_type, key_index, data_type, data_index, parent):
			key = "table"
			is_property = False
			if parent != None:
				key = _get_data(key_type, key_index)
				is_property = True

				if key_type == DieselScriptDataEnums.ITEM_NUMBER:
					key = "value_node"
					is_property = False

					if data_type == DieselScriptDataEnums.ITEM_TABLE:
						key = "table"

			if data_type == DieselScriptDataEnums.ITEM_TABLE:
				is_property = False

			element = None
			if parent != None:
				if not is_property:
					element = SubElement(parent, key)
			else:
				element = Element(key)
				element.set("version", "5")

			data = _get_data(data_type, data_index)

			if data_type != DieselScriptDataEnums.ITEM_TABLE:
				if type(data) is tuple:
					data = map(str, data)
					data = " ".join(data)
				elif type(data) is bool:
					data = str(data).lower()
				elif type(data) is float:
					if data.is_integer():
						data = int(data)
					data = str(data)
				else:
					data = str(data)

				if is_property:
					parent.set(key, data)
				else:
					element.set("value", data)
			else:
				if data[0] != -1:
					element.set("_meta", data[0])

				for item_info in data[1]:
					_generate_tree(item_info[0], item_info[1], item_info[2], item_info[3], element)

			return element

		data = _get_data(self.root_type, self.root_index)

		root_element = _generate_tree(None, None, self.root_type, self.root_index, None)

		return tostring(root_element).decode("utf-8")

	@custom_xml.setter
	def custom_xml(self, value):
		None